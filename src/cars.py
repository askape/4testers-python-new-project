import datetime
class Car:
    def __init__(self, brand, colour, production_year):
        self.brand = brand
        self.colour = colour
        self.production_year = production_year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance

    def get_age(self):
        return datetime.datetime.now().year - self.production_year

    def repaint(self, new_colour):
        self.colour = new_colour

if __name__ == '__main__':
    car1 = Car('Toyota', 'black', 2001)
    car2 = Car('Honda', 'yellow', 2021)
    car3 = Car('Audi', 'red', 1997)
    print(car3.get_age())
    car3.drive(9)
    print(car3.mileage)
    car2.repaint('green')
    print(car2.colour)
