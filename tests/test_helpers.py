from src.helpers import is_adult


def test_is_not_adult_for_age_17():
    assert is_adult(17) == False


def test_is_adult_for_age_18():
    assert is_adult(18) == True


def test_is_adult_for_age_19():
    assert is_adult(19) == True
